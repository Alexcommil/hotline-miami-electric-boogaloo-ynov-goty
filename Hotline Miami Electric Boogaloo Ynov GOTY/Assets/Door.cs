﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>(); 
    }

    // Update is called once per frame
    void Update()
    {
       // Debug.Log(rb.velocity.magnitude);
    }
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("door");
        if(other.CompareTag("Player"))
        {
            rb.AddForce((transform.position - other.gameObject.transform.position)*1900);
        }
        if(other.CompareTag("Enemy") && rb.velocity.magnitude > 3)
        {
            other.GetComponent<Enemy>().getHurt(1, 0);
                
        }
    }
}
