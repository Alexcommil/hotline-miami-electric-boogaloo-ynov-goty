﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    Vector3 vec;
    Vector3 mod = new Vector3(0,1,0);
    float timer;

    
    // Start is called before the first frame update
    void Start()
    {
        vec = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, vec + mod, Time.deltaTime);

        timer += Time.deltaTime;

        if (timer > .7)
        {
            mod = -mod;
            timer = 0;
        }
    }
}
