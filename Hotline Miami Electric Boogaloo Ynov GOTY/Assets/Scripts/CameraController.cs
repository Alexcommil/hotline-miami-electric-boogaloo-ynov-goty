﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    
    bool Following = true;
    private Camera cam;
    float lerpAmount;
    // Start is called before the first frame update
    GameObject Player;
    float zVal = 0f;
    float timer = 0.1f;
    float mod = 0.1f;
    void Start()
    {
        cam = Camera.main;
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            Following = false;
        }
        else
        {
            Following = true;
        }

        if (Following)
        {
            Follow();
        }
        else
        {
            Explore();
        }

        if(Player.GetComponent<PlayerController>().moving)
        {
            
            Vector3 rotation = new Vector3(0, 0, zVal);
            transform.eulerAngles = rotation;

            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                zVal += mod;
                timer = 0.1f;
            }
                if (transform.eulerAngles.z >= 5f && transform.eulerAngles.z < 7f)
                    mod = -.1f;
                else if (transform.eulerAngles.z < 355f && transform.eulerAngles.z > 353f)
                    mod = .1f;
            


        }
    }

    public void toggleFollow()
    {
        Following = !Following;
    }
    void Follow()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z) / 100 - new Vector3(Screen.width / 2, Screen.height / 2, 0) / 100;
        Vector3 newPos = Player.transform.position + mousePos.normalized*.2f;
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * 15);
    }
    void Explore()
    {
        

        Vector3 mousePos =new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z)/100 -  new Vector3(Screen.width/2,Screen.height/2,0) /100  ;
        Vector3 newPos = Player.transform.position + mousePos;
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * 13);
    }
    
}
