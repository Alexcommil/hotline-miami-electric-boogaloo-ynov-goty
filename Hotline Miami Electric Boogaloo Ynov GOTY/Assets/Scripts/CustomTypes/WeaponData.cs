﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="WeaponData", menuName ="Items/Weapons")]
public class WeaponData : ScriptableObject
{
    public string Name;

    public bool isMelee;

    public bool canExecute;

    public Animation executeAnimation;

    public Animation swingAnimation;

    public float SwingSpeed;

    public float KnockBack;

    public float Damage;

    public float Range;

    public float Spread;

    public int ProjectileAmmount;

    public float ProjectileSpeed ;

    public int MaxAmmo;

    public Sprite onGround;

    public Sprite inHands;

    public int RateOfFire; 

    

}
