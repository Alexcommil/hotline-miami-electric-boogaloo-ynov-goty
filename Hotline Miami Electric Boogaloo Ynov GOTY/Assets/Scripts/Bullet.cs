﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    public float damage;
    public float range;
    

    public Bullet(WeaponData data)
    {
        speed = data.ProjectileSpeed;
        damage = data.Damage;
        range = data.Range;
    }


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += transform.right * speed * Time.deltaTime;
        //Debug.Log("ntm");
    }

    internal void getData(WeaponData data)
    {
        speed = data.ProjectileSpeed;
        damage = data.Damage;
        range = data.Range;
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("alo");
        if (other.gameObject.CompareTag("Wall") || other.gameObject.CompareTag("Door"))
        {
            Debug.Log("alo2");
            Destroy(this.gameObject);
        }
        else if (other.gameObject.CompareTag("Enemy"))
        {

            //ouch
            other.gameObject.GetComponent<Enemy>().getHurt(damage, 0);
            Destroy(this.gameObject);
        }
    }
   
}
