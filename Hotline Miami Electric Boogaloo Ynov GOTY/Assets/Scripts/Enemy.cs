﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    public float walkingSpeed = 7.5f;
    public float runningSpeed = 11.5f;
    public bool canMove = true;
    public bool moving = false;
    public bool isLocked = false;
    public bool dead = false;
    public Weapon HeldWeapon;
    public Weapon WeaponInRange;
    public Sprite againstWall;
    public Sprite onGround;
    public Sprite Standing;
    public Sprite Dead;
    public float Health = 4;
    float timer = 0;
    public bool down = false;
    SpriteRenderer spriteRenderer;
    public Collider collider;
    bool seePlayer = true;
    GameObject Player;
    RaycastHit hit;
    int layer = 1 << 10;
    private int layerMask = 1 << 9;

    /// <summary>
    /// /enemy to lock
    /// </summary>
    Vector3 moveDirection = Vector3.zero;
    //CharacterController characterController;
    GameObject Torso;

    void Start()
    {
        //characterController = GetComponent<CharacterController>();
        //Torso = this.transform.GetChild(0).gameObject;
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<Collider>();
        Player = GameObject.FindGameObjectWithTag("Player");
        layerMask = layerMask | layer;

    }

    // Update is called once per frame
    void Update()
    {

        if (EventManager.gameRunning)
        {

            if (!EventManager.gamePaused && !dead)
            {
                // mvt
                Vector3 up = transform.TransformDirection(Vector3.up);
                Vector3 right = transform.TransformDirection(Vector3.right);
                // shift to sprint

                if (moveDirection != Vector3.zero)
                {
                    moving = true;
                }
                else
                {
                    moving = false;
                }


                if (down)
                {
                    timer -= Time.deltaTime;
                    if (timer <= 0)
                    {
                        getUp();
                    }
                }

                if (!down && seePlayer)
                {
                    RotateToEnnemy();
                }

                // Move 
                //characterController.Move(moveDirection * Time.deltaTime);

                // Player and Camera rotation




                //attacks



                //interaction


                //if (Input.GetKeyDown(KeyCode.F) && button != null)
                //{
                //    button.Interact();
                //}




            }
            //pause





            //items interactions










        }
    }
    private void LateUpdate()
    {
        

        
        if (Physics.Raycast(transform.position, Player.transform.position - transform.position, out hit, 100f))
        {
            
            if (hit.collider.CompareTag("Player"))
            {
                seePlayer = true;
                

            }
            else
            {
                seePlayer = false;
            }
            
        }
        
    }

        

    private void getUp()
    {
        down = false;
        spriteRenderer.sprite = Standing;
        canMove = true;
        Debug.Log("not oof");
        collider.enabled = true;

    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Weapon"))
        {
            Debug.Log("triggerenter");
            WeaponInRange = other.gameObject.GetComponent<Weapon>();

        }
    }
    private void OnTriggerExit(Collider other)
    {

        if (other.CompareTag("Weapon"))
        {
            Debug.Log("triggerexit");

            WeaponInRange = null;

        }
    }
    public void Attack()
    {
        if (HeldWeapon != null)
        {
            HeldWeapon.Fire(transform);
        }
        else
        {
            //BARE HANDED ATTACK LOL
        }
    }
    
    public void getHurt(float damage, float knockback)
    {
        if(!down)
        { 
        Health -= damage;
            if (Health < 1)
            {
                Die();

            }
            else
            {
                KnockedDown();
            }

            
        }
    }

    public void Die()
    {   spriteRenderer.sprite = Dead;
        canMove = false;
        collider.enabled = false;
        
        dead = true;
    }
    void RotateToEnnemy()
    {
        Vector3 EnnemyPos = Player.transform.position;//todo when ennemy created
        transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2((EnnemyPos.y - transform.position.y), (EnnemyPos.x - transform.position.x)) * Mathf.Rad2Deg);
    }
    public void KnockedDown()
    {
        Debug.Log("oof");
        timer = UnityEngine.Random.Range(2,5);
        spriteRenderer.sprite = onGround;
        down = true;
        canMove = false;
        collider.enabled = false;


    }

}

