﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    public float walkingSpeed = 7.5f;
    public bool canMove = true;
    public bool moving = false;
    public bool isLocked = false;
    public Weapon HeldWeapon;
    public Weapon WeaponInRange;
    float click;
    bool dropped;

    private int layerMask = 1 << 8;

    /// <summary>
    /// /enemy to lock
    /// </summary>
    Vector3 moveDirection = Vector3.zero;
    CharacterController characterController;
    public GameObject Torso;
    Camera cam;
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        cam = Camera.main;
        Torso = this.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {

        if (EventManager.gameRunning)
        {

            if (!EventManager.gamePaused)
            {
                // mvt
                Vector3 up = transform.TransformDirection(Vector3.up);
                Vector3 right = transform.TransformDirection(Vector3.right);
                // shift to sprint
                float curSpeedX = canMove ? (walkingSpeed) * Input.GetAxis("Vertical") : 0;
                float curSpeedY = canMove ? (walkingSpeed) * Input.GetAxis("Horizontal") : 0;
                float movementDirectionY = moveDirection.y;
                moveDirection = (up * curSpeedX) + (right * curSpeedY);
                if (moveDirection != Vector3.zero)
                {
                    moving = true;
                }
                else
                {
                    moving = false;
                }


                



                // Move 
                characterController.Move(moveDirection * Time.deltaTime);

                // Player and Camera rotation
                if (!isLocked)
                {
                    RotateToCam();
                }
                else
                {
                    RotateToEnnemy();
                }



                //attacks
                if (Input.GetButton("Fire1"))
                {
                    Attack();
                }


                //interaction


                //if (Input.GetKeyDown(KeyCode.F) && button != null)
                //{
                //    button.Interact();
                //}




            }
            //pause
            if (Input.GetKeyDown("p"))
            {
                if (!EventManager.gamePaused)
                {
                    EventManager.TogglePause();
                }
                //else
                //    GameMenuManager.ResumeGame();
            }

            if (Input.GetButton("Fire2"))
            {
                click += Time.deltaTime;
                if(click > 1.2 && HeldWeapon)
                {
                    HeldWeapon.Drop(this);
                    dropped = true;
                    click = 0;
                }
            }

            if (Input.GetButtonUp("Fire2"))
            {


            
            if (HeldWeapon)
            {
                HeldWeapon.Throw(this);
            }
            if (WeaponInRange && !dropped)
            {

                WeaponInRange.PickUp(this);
                    
            }
                dropped = false;
            }



            //items interactions










        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Weapon"))
        {
            Debug.Log("triggerenter");
            WeaponInRange = other.gameObject.GetComponent<Weapon>();

        }
    }
    private void OnTriggerExit(Collider other)
    {

        if (other.CompareTag("Weapon"))
        {
            Debug.Log("triggerexit");

            WeaponInRange = null;

        }
    }
    public void Attack()
    {
        if (HeldWeapon != null)
        {
            HeldWeapon.Fire(Torso.transform);
        }
        else
        {
            //BARE HANDED ATTACK LOL
        }
    }
    void RotateToCam()
    {
        Vector3 mousePos = new Vector3(cam.ScreenToWorldPoint(Input.mousePosition).x, cam.ScreenToWorldPoint(Input.mousePosition).y, cam.ScreenToWorldPoint(Input.mousePosition).z);
        Torso.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2((mousePos.y - transform.position.y), (mousePos.x - transform.position.x)) * Mathf.Rad2Deg);
    }
    void RotateToEnnemy()
    {
        Vector3 EnnemyPos = Vector3.zero;//todo when ennemy created
        Torso.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2((EnnemyPos.y - transform.position.y), (EnnemyPos.x - transform.position.x)) * Mathf.Rad2Deg);
    }

    public void getHurt(float damage, float knockback)
    {

    }

}

