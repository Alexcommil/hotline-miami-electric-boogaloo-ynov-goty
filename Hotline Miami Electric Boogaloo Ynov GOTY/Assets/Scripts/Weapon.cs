﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    // Start is called before the first frame update
    public WeaponData weaponData;
    public Sprite onGround;
    public Sprite inHands;
    public bool isHeld = false;
    public Bullet bullet;
    public SpriteRenderer SpriteRenderer;
    public Collider collider;
    public GameObject holder;
    Rigidbody rb;
    bool canFire = true;
    float timer;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        onGround = weaponData.onGround;
        inHands = weaponData.inHands;
        if(!weaponData.isMelee)
        {
            bullet.getData(weaponData); 
        }
        SpriteRenderer.sprite = onGround;

    }

    // Update is called once per frame
    void Update() //jspfrers
    {
        if(isHeld)
        {
        transform.position = holder.transform.position;
            transform.rotation = holder.transform.GetChild(0).gameObject.transform.rotation;
        }
        if (rb.velocity.magnitude < 0.5f)
        {
            rb.angularVelocity = Vector3.zero;
            rb.velocity = Vector3.zero;
        }

        if(!canFire)
        {
            timer += Time.deltaTime;
            if(timer > (float)1/weaponData.RateOfFire)
            {
                canFire = true;
                timer = 0;
            }
        }
    }

    public void Fire(Transform playerTransform)
    {
        if (canFire)
        { 
        Vector3 pos = playerTransform.position;
        Vector3 dir = playerTransform.right;
        if (weaponData.isMelee) //swing the weapon
        {

            //khung
        }
        else //fire bullets
        {
            for (int i = 0; i < weaponData.ProjectileAmmount; i++)
            {
                Vector3 rot = transform.rotation.eulerAngles + new Vector3(0, 0, UnityEngine.Random.Range(-weaponData.Spread, weaponData.Spread));
                    
                Bullet projectile = Instantiate(bullet, pos + dir, Quaternion.Euler(rot) );

            }

        }
        canFire = false;
        }
    }
    public void PickUp(PlayerController player)
    {
        collider.enabled = false;
        player.HeldWeapon = this;
        SpriteRenderer.sprite = inHands;
        holder = player.gameObject;
        player.WeaponInRange = null;
        Debug.Log("pickup");
        isHeld = true;
    }
    public void Drop(PlayerController player)
    {
        Debug.Log("drop");
        collider.enabled = true;
        player.HeldWeapon = null;
        SpriteRenderer.sprite = onGround;
        transform.position = player.transform.position;
        isHeld = false;
        rb.velocity = Vector3.zero;
    }

    internal void Throw(PlayerController player)
    {
        Debug.Log("throw");
        collider.enabled = true;
        player.HeldWeapon = null;
        SpriteRenderer.sprite = onGround;
        transform.position = player.transform.position;
        rb.velocity = player.Torso.transform.right * 15;
        rb.angularVelocity = new Vector3(0, 0, 15); 
        
        isHeld = false;
    }
    private void OnTriggerEnter(Collider other)
    {
       
        if (other.gameObject.CompareTag("Enemy") && rb.velocity.magnitude > 6)
        {

            //ouch
            other.gameObject.GetComponent<Enemy>().getHurt(weaponData.Damage, 0);
            rb.velocity -= rb.velocity * 0.9f;
            
        }
        if (other.gameObject.CompareTag("Door"))
        {
            rb.velocity *= rb.velocity.magnitude * 0.0f;
        }
        

    }


    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("AAAAAAAAAAAAAAAA");
        if (collision.gameObject.CompareTag("Wall"))
        {
             rb.velocity = collision.contacts[0].normal * rb.velocity.magnitude * 0.2f;
        }
    }
}
